'use strict';

import koa from 'koa'
import router from './api'
import mongoose from 'mongoose'
import bodyParser from 'koa-bodyparser';

mongoose.connect('mongodb://mongo:27017/openShelter', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

const app = new koa()

app.use(bodyParser())
app.use(router())


app.listen(3000)

export default app  