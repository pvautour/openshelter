import combineRouters from 'koa-combine-routers'
import rootRouter from './root'
import userRouter from './user'
import animalRouter from './animal'

const router = combineRouters(
  rootRouter,
  userRouter,
  animalRouter
)

export default router